<?php
    require_once "functions.php";

    $functions = new Calculator();

     $number1 = $_POST["number1"];
     $number2 = $_POST["number2"];
     $operator = $_POST["operator"];

     if($operator == "+"){
        print_r(json_encode($functions->sum($number1,$number2)));
     }elseif($operator == "-"){
        print_r(json_encode($functions->subtraction($number1,$number2)));
     }elseif($operator == "*"){
        print_r(json_encode($functions->multiplication($number1,$number2))); 
     }elseif($operator == "/"){
        print_r(json_encode($functions->division($number1,$number2)));
    }
?>