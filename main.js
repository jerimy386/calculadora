$(document).ready(function(){
    var value1 = $("#value1");
    var value2 = $("#value2");
    var operation = $("#select-operation");
    var total = $("#total");

    $("#btnTotal").click(function(){
       var data = {"number1":value1.val(),"number2":value2.val(),"operator":operation.val()};

        $.ajax({
            url: "invoke.php",
            method:"POST",
            data: data,
            dataType: "json",
            success: function(data){
                $("#total").val(data);
            }
        });
    });
});